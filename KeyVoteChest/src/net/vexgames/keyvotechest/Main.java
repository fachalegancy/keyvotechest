package net.vexgames.keyvotechest;

import java.io.IOException;

import net.milkbowl.vault.economy.Economy;
import net.vexgames.keyvotechest.Updater.UpdateResult;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

public class Main extends JavaPlugin{
	static Main plugin;
	public static Economy econ = null;
	public boolean updatefound = false;
	public void onEnable(){
		getConfig().options().copyDefaults(true);
		saveConfig();
		saveDefaultConfig();
		if(getConfig().getBoolean("autoupdate")){
			Updater updater = new Updater(this, 88469, this.getFile(), Updater.UpdateType.DEFAULT, false);
			if (updater.getResult() == UpdateResult.UPDATE_AVAILABLE) {
				updatefound = true;
				System.out.println("[KeyVoteChest] New update available, update at: http://dev.bukkit.org/bukkit-plugins/keyvotechest/");
			}
		}
		if (!setupEconomy() ) {
            System.out.println("[KeyVoteChest] Disabled due to no Vault dependency found!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
		if(!getConfig().isSet("metrics")){
			getConfig().set("metrics", true);
			saveConfig();
		}
		if(getConfig().getBoolean("metrics")){
			 try {
			        MetricsLite metrics = new MetricsLite(this);
			        metrics.start();
			    } catch (IOException e) {
			        // Failed to submit the stats :-(
			    }
		}
		plugin = this;
		System.out.println("[KVC] Enabling KeyVoteChest v"+Bukkit.getPluginManager().getPlugin("KeyVoteChest").getDescription().getVersion());
		Bukkit.getPluginManager().registerEvents(new ClickListener(), this);
	}
	
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
		if(command.getName().equalsIgnoreCase("chestreload")){
			if(!sender.hasPermission("kvc.reload")){
				sender.sendMessage(getConfig().getString("msg.no_permission"));
				return false;
			}
			reloadConfig();
			sender.sendMessage("Config reloaded!");
			return false;
		}
		if(command.getName().equalsIgnoreCase("givekey")){
			if(!sender.hasPermission("kvc.givekey")){
				sender.sendMessage(getConfig().getString("msg.no_permission"));
				return false;
			}
			if(args.length != 1){
				sender.sendMessage("Use /givekey <player name>");
				return false;
			}
			if(Bukkit.getPlayerExact(args[0]) == null){
				sender.sendMessage("That player is not online!");
				return false;
			}
			ItemSelector its = new ItemSelector();
			its.darkey(Bukkit.getPlayerExact(args[0]));
		}
		
		if(command.getName().equalsIgnoreCase("vote")){
			sender.sendMessage(getConfig().getString("msg.vote_links"));
			int x = 1;
			for(String link : getConfig().getStringList("votelinks")){
				sender.sendMessage("§2["+x+"]"+" §6"+link);
				x++;
			}
		}
		
		return false;
	}
	
	

}
