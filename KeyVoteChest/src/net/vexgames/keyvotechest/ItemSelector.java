package net.vexgames.keyvotechest;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemSelector {
	
	public void daritem(Player p){
		Random r = new Random();
		ItemStack is = null;
		int maximo = Main.plugin.getConfig().getStringList("items").size()-1;
		int rand = r.nextInt(maximo+1);
		if(Main.plugin.getConfig().getStringList("items").get(rand).contains(":")){
			int where = Main.plugin.getConfig().getStringList("items").get(rand).indexOf(":");
			String numero = Main.plugin.getConfig().getStringList("items").get(rand).substring(where+1);
			String bloco = Main.plugin.getConfig().getStringList("items").get(rand).substring(0, where);
			is = new ItemStack(Material.getMaterial(Integer.valueOf(bloco)), 1, Short.valueOf(numero));
		}else{
			is = new ItemStack(Material.getMaterial(Integer.valueOf(Main.plugin.getConfig().getStringList("items").get(rand))));
		}
		if(Main.plugin.getConfig().getStringList("items").get(rand).contains(":")){
			int onde = Main.plugin.getConfig().getStringList("items").get(rand).indexOf(":");
			String numero = Main.plugin.getConfig().getStringList("items").get(rand).substring(onde+1);
			
			
		}
		p.getInventory().addItem(is);
		p.updateInventory();
	}
	
	public void darkey(Player p){
		ItemStack is = new ItemStack(Material.TRIPWIRE_HOOK);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName("§a§l"+Main.plugin.getConfig().getString("itemname"));
		List<String> lore = Arrays.asList("§6"+Main.plugin.getConfig().getString("nome"));
		im.setLore(lore);
		is.setItemMeta(im);
		p.getInventory().addItem(is);
		

		
	}
	
	public void darmoney(Player p ){
		Main.plugin.econ.depositPlayer(Bukkit.getPlayerExact(p.getName().toString()), Main.plugin.getConfig().getDouble("money"));
	}

}
