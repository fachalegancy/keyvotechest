package net.vexgames.keyvotechest;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class ClickListener implements Listener{
	
	@EventHandler
	public void onBau(InventoryOpenEvent e){
		if(e.getInventory().getName().contains("KeyChest")){
			if(e.getPlayer().getItemInHand() != null){
				if(e.getPlayer().getItemInHand().getType() == Material.TRIPWIRE_HOOK){
					
					if(e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains(Main.plugin.getConfig().getString("itemname")) && e.getPlayer().getItemInHand().getItemMeta().getLore().get(0).contains(Main.plugin.getConfig().getString("nome"))){
						e.setCancelled(true);
						e.getView().close();
						ItemSelector its = new ItemSelector();
						if(Main.plugin.getConfig().getString("tipo").equalsIgnoreCase("item")){
						its.daritem((Player) e.getPlayer());
						}
						if(Main.plugin.getConfig().getString("tipo").equalsIgnoreCase("money")){
							its.darmoney((Player) e.getPlayer());
						}
						ItemStack istp = new ItemStack(Material.TRIPWIRE_HOOK);
						istp.setAmount(1);
						e.getPlayer().getInventory().remove(istp);
						((CommandSender) e.getPlayer()).sendMessage(Main.plugin.getConfig().getString("msg.done"));
					}else{
						e.setCancelled(true);
						e.getView().close();
						((CommandSender) e.getPlayer()).sendMessage(Main.plugin.getConfig().getString("msg.must_use_key"));
					}
					
				}else{
					e.setCancelled(true);
					e.getView().close();
					((CommandSender) e.getPlayer()).sendMessage(Main.plugin.getConfig().getString("msg.must_use_key"));
				}
			}else{
				e.setCancelled(true);
				e.getView().close();
				((CommandSender) e.getPlayer()).sendMessage(Main.plugin.getConfig().getString("msg.must_use_key"));
			}
		}
	}
	
	@EventHandler
	public void onplace(BlockPlaceEvent e){
		if(e.getBlock().getType() == Material.CHEST){
			if(e.getPlayer().getItemInHand().hasItemMeta()){
				if(e.getPlayer().getItemInHand().getItemMeta().getDisplayName().toString().toLowerCase().contains("KeyChest".toLowerCase())){
				if(!e.getPlayer().hasPermission("kvc.deploy")){
					e.setCancelled(true);
					e.getPlayer().sendMessage(Main.plugin.getConfig().getString("msg.no_permission"));
					return;
				}
				e.getPlayer().sendMessage("§6The bomb has been planted! §2(Vote Chest placed :P)");
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent e){
		if(e.getPlayer().isOp()){
			if(Main.plugin.updatefound){
				e.getPlayer().sendMessage("§c§l[WARNING] §6New update found for KeyVoteChest, please update at: http://dev.bukkit.org/bukkit-plugins/keyvotechest/");
			}
		}
	}

}
